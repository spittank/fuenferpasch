// @flow
/* eslint-disable func-names */
/* eslint-disable no-console */
import {createSkill} from './createSkill';
import {DynamoDbPersistenceAdapter} from 'ask-sdk-dynamodb-persistence-adapter';
import {localizationInterceptor} from './localizationInterceptor';

export const handler = createSkill(DynamoDbPersistenceAdapter, [localizationInterceptor]);