import {deepGetOrDefault} from './deepGetOrDefault';

describe('deepGetOrDefault', () => {

    it('should return the default for an empty object', () => {
        const defaultVal = 'DEFAULT';
        const val = deepGetOrDefault({}, defaultVal, 'any', 'argument');

        expect(val).toEqual(defaultVal);
    });

    it('should return the default for if the given object is undefined', () => {
        const defaultVal = 'DEFAULT';
        const val = deepGetOrDefault(undefined, defaultVal, 'any', 'argument');

        expect(val).toEqual(defaultVal);
    });

    it('should return the default for if the given object does not contain the given property', () => {
        const defaultVal = 'DEFAULT';
        const val = deepGetOrDefault({person: {name: 'Hugo'}}, defaultVal, 'person', 'surname');

        expect(val).toEqual(defaultVal);
    });

    it('should return value of the object-tree given the property path for an existing string property', () => {
        const defaultVal = 'DEFAULT';
        const val = deepGetOrDefault({person: {name: 'Hugo'}}, defaultVal, 'person', 'name');

        expect(val).toEqual('Hugo');
    });

    it('should return value of the object-tree given the property path for an existing object property', () => {
        const defaultVal = 'DEFAULT';
        const val = deepGetOrDefault({person: {name: 'Hugo'}}, defaultVal, 'person');

        expect(val).toEqual({name: 'Hugo'});
    });
});