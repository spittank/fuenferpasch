// @flow
import type {Attributes, HandlerInput, Intent, ListManagementServiceClient, ResponseBuilder} from '../own-flow-types/ask-sdk/inputHandler';
import {AlexaListItem, AlexaListsMetadata, CreateListItemRequest} from 'ask-sdk-model';

export type MockResponse = ResponseBuilder & {
    speechText?: string,
    repromptText?: string,
    simpleCardText?: string,
    simpleCardTitle?: string,
    shouldEndSession?: boolean,
    requestedPermissions?: string[],
    updatedIntent?: Intent,
    hasDelegateDirective?: boolean
};

export type MockHandlerInput = HandlerInput & {
    setMockApiAccessToken: string => void,
    //retrieve all list items created with the serviceClient provided in this request
    getMockListItemCreateRequests: () => CreateListItemRequest[]
};

const getMockResponseBuilder = (): $Shape<MockResponse> => {
    const that: MockResponse = {
        speak: (speechText: string) => {
            that.speechText = speechText;
            return that;
        },
        reprompt: (repromptText: string) => {
            that.repromptText = repromptText;
            return that;
        },
        withSimpleCard: (title: string, cardText: string) => {
            that.simpleCardTitle = title;
            that.simpleCardText = cardText;
            return that;
        },
        withShouldEndSession: (shouldEndSession: boolean) => {
            that.shouldEndSession = shouldEndSession;
            return that;
        },
        withAskForPermissionsConsentCard: (permissions: string[]) => {
            that.requestedPermissions = permissions;
            return that;
        },
        addDelegateDirective: (updatedIntent): MockResponse => {
            const mockResponse: mockResponse = {
                ...that,
                updatedIntent
            };
            return mockResponse;
        },
        getResponse: () => that,
        speechText: '',
        repromptText: '',
        simpleCardText: '',
        simpleCardTitle: '',
        shouldEndSession: undefined,
        updatedIntent: undefined,
        hasDelegateDirective: false
    };
    return that;
};

// In a request we get plain javascript objects, stripped from all their class properties
// To simulate this we convert all objects to JSON and back
export const simulateDeserialization = function<T>(sourceObject: T): T {
    return JSON.parse(JSON.stringify(sourceObject))
};

export function getMockHandlerInputForLaunch(sessionAttributes: Attributes  = {}, persistentAttributes: Attributes = {}): MockHandlerInput {
    const that = getMockHandlerInput(sessionAttributes, persistentAttributes);
    that.requestEnvelope.request = {
        type: 'LaunchRequest'
    };
    return that;
}

export function getMockHandlerInputForIntent(name: string, sessionAttributes: Attributes  = {}, slots: any = {}, persistentAttributes: Attributes = {}): MockHandlerInput {
    const that = getMockHandlerInput(sessionAttributes, persistentAttributes);
    that.requestEnvelope.request = {
        type: 'IntentRequest',
        intent: {
            name,
            slots
        }
    };
    return that;
}

function getMockHandlerInput(sessionAttributes: Attributes  = {}, persistentAttributes: Attributes = {}): MockHandlerInput {
    const deserializedSessionAttributes = simulateDeserialization(sessionAttributes);
    const deserializedPersistentAttributes = simulateDeserialization(persistentAttributes);
    const mockListMetadata = {
        lists: [{
            'listId': 'defaultShoppingListId',
            name: 'Alexa shopping list',
            state: 'active',
            statusMap: [],
            version: 1
        }]
    };
    const listItemCreateRequests: CreateListItemRequest[] = [];
    const mockListManagementServiceClient: ListManagementServiceClient = {
        getListsMetadata: () => Promise.resolve(mockListMetadata),
        createListItem: (listId: string, createListItemRequest: CreateListItemRequest): Promise<AlexaListItem> => {
            listItemCreateRequests.push(createListItemRequest);
            return Promise.resolve();
        }
    };
    const that: MockHandlerInput = {
        requestEnvelope: {
            context: {
                System: {
                    user: {
                        permissions: {
                        }
                    }
                }
            },
            request: {
                type: 'LaunchRequest'
            }
        },
        attributesManager: {
            getSessionAttributes: () => deserializedSessionAttributes,
            setSessionAttributes: jest.fn(),
            getPersistentAttributes: () => Promise.resolve(deserializedPersistentAttributes),
            setPersistentAttributes: jest.fn(),
            savePersistentAttributes: () => Promise.resolve()
        },
        serviceClientFactory: {
            apiConfiguration: {
                apiEndpoint: 'initially wrong endpoint due to bug in aws'
            },
            getListManagementServiceClient: jest.fn(() => mockListManagementServiceClient)
        },
        responseBuilder: getMockResponseBuilder(),
        setMockApiAccessToken: token => {
            that.requestEnvelope.context.System.apiAccessToken = token;
        },
        getMockListItemCreateRequests: () => listItemCreateRequests
    };
    return that;
}