// @flow
import fs from 'fs';

export const getVoiceUiModel = (locale: string) => {
    const json = fs.readFileSync(`voiceUi/interactionModel.${locale}.json`, 'utf-8');

    return JSON.parse(json);
};
