// @flow
import fs from 'fs';
import {findMatchingIntent, getAllUtterances} from './mockVoiceService';


describe('getAllUtterances should', () => {
    const json = fs.readFileSync('voiceUi/simpleInteractionModelForTests.json', 'utf-8');
    const voiceUiModel = JSON.parse(json);
    const allUtterances = getAllUtterances(voiceUiModel.interactionModel);

    it('return a regexp for an utterance without slots matching only the complete text', () => {
        expect(allUtterances.find(u => u.intentName === 'intent without slot')).toEqual({
            intentName: 'intent without slot',
            slots: [],
            utteranceRegExp: '^An utterance without slot$'
        });
    });

    it('replace an amazon slot type with a regexp matching arbitrary text', () => {
        expect(allUtterances.find(u => u.intentName === 'intent with AMAZON.Food type slot')).toEqual({
            intentName: 'intent with AMAZON.Food type slot',
            slots: ['food'],
            utteranceRegExp: '^Give me the (.*)$'
        });
    });

    it('replace a custom slot type with a regexp matching all possible slot values', () => {
        expect(allUtterances.find(u => u.intentName === 'intent with custom type slot')).toEqual({
            intentName: 'intent with custom type slot',
            slots: ['foobar'],
            utteranceRegExp: '^Tell me about (foo|bar|foobar)$'
        });
    });

    it('remove utterance with consisting solely of an amazon slot type because the resulting regexp would match everything', () => {
        expect(allUtterances.find(u => u.intentName === 'intent with only one utterance with an Amazon slot type')).not.toBeDefined();
    });
});

describe('findMatchingIntent should', () => {
    const json = fs.readFileSync('voiceUi/simpleInteractionModelForTests.json', 'utf-8');
    const voiceUiModel = JSON.parse(json);
    const allUtterances = getAllUtterances(voiceUiModel.interactionModel);

    it('return undefined if there is no match', () => {
        expect(findMatchingIntent(allUtterances, 'I shall not be matched')).not.toBeDefined();
    });

    it('return an intentInvocation for a matching utterance without slots', () => {
        expect(findMatchingIntent(allUtterances, 'An utterance without slot')).toEqual({
            intentName: 'intent without slot',
            slots: [],
            utteranceRegExp: '^An utterance without slot$'
        });
    });

    it('return an intentInvocation with matched slots for a matching utterance with custom slot', () => {
        expect(findMatchingIntent(allUtterances, 'Tell me about foo')).toEqual({
            intentName: 'intent with custom type slot',
            slots: [{name: 'foobar', value: 'foo'}],
            utteranceRegExp: '^Tell me about (foo|bar|foobar)$'
        });
    });

    it('return an intentInvocation with matched slots for a matching utterance with amazon slot', () => {
        expect(findMatchingIntent(allUtterances, 'Give me the coffee')).toEqual({
            intentName: 'intent with AMAZON.Food type slot',
            slots: [{name: 'food', value: 'coffee'}],
            utteranceRegExp: '^Give me the (.*)$'
        });
    });
});