// @flow
import {deepGetOrDefault} from '../../deepGetOrDefault';
import type {IntentModel, InteractionModel, SlotModel, SlotType} from '../../own-flow-types/ask-sdk/interactionModel';
import type {Slot} from '../../own-flow-types/ask-sdk/inputHandler';

export type Utterance = {
    intentName: string,
    utteranceRegExp: string,
    slots: string[]
};

export function getAllUtterances(interactionModel: InteractionModel): Utterance[] {
    const intents: IntentModel[] = interactionModel.languageModel.intents;
    return intents.reduce((acc, curIntent) => {
        const intentName = curIntent.name;
        const intentUtterances = curIntent.samples.reduce((accSamples, curSample) => {
            let regexpForUtterance =  curSample;
            let utteranceWithoutSlots = curSample;

            let match;
            const regExp = /{(.*?)}/g;
            const slots: string[] = [];
            while ((match = regExp.exec(curSample)) !== null) {
                if (match) {
                    const slotName = match[0].replace('{', '').replace('}', '');
                    slots.push(slotName);

                    const slotType = getSlotType(interactionModel, curIntent.slots, slotName);
                    const regexpForSlot = slotType ? getRegexpForSlotType(slotType) : '(.*)';

                    const regexpSoFar = regexpForUtterance;
                    regexpForUtterance = regexpForUtterance.replace(match[0], regexpForSlot);
                    utteranceWithoutSlots = regexpSoFar.replace(match[0], '');
                }
            }
            utteranceWithoutSlots = utteranceWithoutSlots.trim();
            if (utteranceWithoutSlots === '') {
                // Ignore utterances consisting only of slots for now due to collisions
                return accSamples;
            }
            const regexpForTotalMatch = `^${regexpForUtterance}$`;
            return [...accSamples, {intentName, utteranceRegExp: regexpForTotalMatch, slots}];
        }, []);
        return [...acc, ...intentUtterances]
    }, []);
}

function getRegexpForSlotType(slotType: SlotType) {
    const values = slotType.values.map(v => deepGetOrDefault(v, '', 'name', 'value')).filter(v => v !== '');
    return `(${values.join('|')})`;
}

function getSlotType(interactionModel: InteractionModel, intentSlots: SlotModel[], slotName: string): ?SlotType {
    const intentSlot = intentSlots.find(s => s.name === slotName);
    const {types} = interactionModel.languageModel;
    return intentSlot ? types.find(t => t.name === intentSlot.type) : undefined;
}

export type IntentInvocation = {
    intentName: string,
    utteranceRegExp: string,
    slots: Slot[]
};

export function findMatchingIntent(allUtterances: Utterance[], utteranceText: string): ?IntentInvocation {
    const matchingUtterance = allUtterances.find(u => {
        const r = new RegExp(u.utteranceRegExp);
        const match = r.exec(utteranceText);

        return match !== null;
    });
    if (!matchingUtterance) return undefined;
    const matches = new RegExp(matchingUtterance.utteranceRegExp).exec(utteranceText);
    if (matches) {
        const slots: Slot[] = matchingUtterance.slots.map((s, i) => ({
            name: s,
            value: matches[i + 1]
        }));
        return matchingUtterance ? {
            intentName: matchingUtterance.intentName,
            utteranceRegExp: matchingUtterance.utteranceRegExp,
            slots
        } : undefined;
    }
    return undefined;
}