// @flow
import sprintf from 'i18next-sprintf-postprocessor';
import {strings_de} from '../i18n/de';
import {strings_en} from '../i18n/en';
import {HandlerInput, RequestInterceptor} from 'ask-sdk-core';
import i18next from 'i18next';

const languageStrings = {
    'en' : strings_en,
    'de' : strings_de,
};

// Based on: https://developer.amazon.com/de/blogs/alexa/post/285a6778-0ed0-4467-a602-d9893eae34d7/how-to-localize-your-alexa-skills
export const localizationInterceptor: RequestInterceptor = {
    process(handlerInput: HandlerInput) {

        const localizationClient = i18next.use(sprintf).init({
            lng: handlerInput.requestEnvelope.request.locale,
            fallbackLng: 'en', // fallback to EN if locale doesn't exist
            resources: languageStrings
        });

        localizationClient.localize = (text, ...values) => {
            const value = i18next.t(text, {
                returnObjects: true,
                postProcess: 'sprintf',
                sprintf: values
            });

            if (Array.isArray(value)) {
                return value[Math.floor(Math.random() * value.length)];
            } else {
                return value;
            }
        };

        const attributes = handlerInput.attributesManager.getRequestAttributes();
        attributes.t = (...args) => localizationClient.localize(...args);
    },
};