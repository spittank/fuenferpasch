// @flow
import type {HandlerInput, ResponseBuilder} from '../../own-flow-types/ask-sdk/inputHandler';

export const ErrorHandler = {
    canHandle(): boolean {
        return true;
    },
    handle(handlerInput: HandlerInput, error: Error): ResponseBuilder {
        console.log(`Error handled: ${error.message}`);
        return handlerInput.responseBuilder
            .speak('Tut mir leid. Es ist ein Fehler aufgetreten. Der Techniker ist informiert')
            .getResponse();
    },
};