import {LaunchRequestHandler} from './launchRequest';
import {getMockHandlerInputForIntent, getMockHandlerInputForLaunch} from '../../test/mockHandlerInput';
import type {MockResponse} from '../../test/mockHandlerInput';

describe('LaunchRequestHandler', () => {
    describe('can handle should', () => {
        it('return true a LaunchRequest', () => {
            expect(LaunchRequestHandler.canHandle(getMockHandlerInputForLaunch())).toEqual(true);
        });

        it('return false for another intent', () => {
            expect(LaunchRequestHandler.canHandle(getMockHandlerInputForIntent('unhandledIntent'))).toEqual(false);
        });
    });

    describe('handle should', () => {
        it('not crash', async () => {
            expect(LaunchRequestHandler.handle(getMockHandlerInputForLaunch())).toBeDefined();
        });

        it('say hello', async () => {
            const response: MockResponse = LaunchRequestHandler.handle(getMockHandlerInputForLaunch());
            expect(response.speechText).toEqual('Willkommen bei Fünferpasch');
        });
    })
});