// @flow
import {SKILL_NAME} from '../../consts';
import type {HandlerInput, Response} from '../../own-flow-types/ask-sdk/inputHandler';

export const LaunchRequestHandler = {
    canHandle(handlerInput: HandlerInput): boolean {
        const request = handlerInput.requestEnvelope.request;
        return request.type === 'LaunchRequest';
    },
    handle(handlerInput: HandlerInput): Response {
        const speech = 'Willkommen bei Fünferpasch';
        return handlerInput.responseBuilder
            .speak(speech)
            .withSimpleCard(SKILL_NAME, speech)
            .getResponse();
    }
};