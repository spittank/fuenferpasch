// @flow
import type {HandlerInput, ResponseBuilder, SessionEndRequest} from '../../own-flow-types/ask-sdk/inputHandler';

export const SessionEndedRequestHandler = {
    canHandle(handlerInput: HandlerInput): boolean {
        const request = handlerInput.requestEnvelope.request;
        return request.type === 'SessionEndedRequest';
    },
    handle(handlerInput: HandlerInput): ResponseBuilder {
        // We know the request is of type SessionEndRequest so we can safely cast via any here
        // (see https://flow.org/en/docs/types/casting/#toc-type-casting-through-any)
        const request: SessionEndRequest = (handlerInput.requestEnvelope.request: any);
        console.log(`Session ended with reason: ${request.reason}`);
        return handlerInput.responseBuilder.getResponse();
    },
};
