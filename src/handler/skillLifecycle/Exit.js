// @flow

import type {HandlerInput, ResponseBuilder} from '../../own-flow-types/ask-sdk/inputHandler';

export const ExitHandler = {
    canHandle(handlerInput: HandlerInput): boolean {
        const request = handlerInput.requestEnvelope.request;
        return request.type === 'IntentRequest'
            && (request.intent.name === 'AMAZON.CancelIntent'
                || request.intent.name === 'AMAZON.StopIntent');
    },
    handle(handlerInput: HandlerInput): ResponseBuilder {
        return handlerInput.responseBuilder
            .speak('Bis bald')
            .getResponse();
    },
};