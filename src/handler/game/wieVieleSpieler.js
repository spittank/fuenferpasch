// @flow
import {HandlerInput} from 'ask-sdk-core';
import {Response} from 'ask-sdk-model';
import {PLAYER_COUNT_KEY, SKILL_NAME} from '../../consts';

export const WieVieleSpielerIntentHandler = {
    canHandle(handlerInput: HandlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest' &&
            handlerInput.requestEnvelope.request.intent.name === 'wieVieleSpieler';
    },
    handle(handlerInput: HandlerInput): Response {
        const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();

        const {t} = handlerInput.attributesManager.getRequestAttributes();

        const numberOfPlayers = sessionAttributes[PLAYER_COUNT_KEY];
        const playersText = numberOfPlayers === '1' ? 'einen' : numberOfPlayers;
        const speechText = t('NO_OF_PLAYER', playersText);

        return handlerInput.responseBuilder
            .speak(speechText)
            .withSimpleCard(SKILL_NAME, speechText)
            .withShouldEndSession(false)
            .getResponse();
    }
};