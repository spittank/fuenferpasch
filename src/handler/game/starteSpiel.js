// @flow
import {HandlerInput} from 'ask-sdk-core';
import {Response} from 'ask-sdk-model';
import {PLAYER_COUNT_KEY, SKILL_NAME} from '../../consts';
import {deepGetOrDefault} from '../../deepGetOrDefault';

export const StarteSpielIntentHandler = {
    canHandle(handlerInput: HandlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest' &&
            handlerInput.requestEnvelope.request.intent.name === 'starteSpiel';
    },
    handle(handlerInput: HandlerInput): Response {
        const numberOfPlayers = deepGetOrDefault(handlerInput, '1', 'requestEnvelope', 'request', 'intent', 'slots', 'spieleranzahl', 'value');

        const {t} = handlerInput.attributesManager.getRequestAttributes();

        const playersText = numberOfPlayers === '1' ? 'einen' : numberOfPlayers;
        const speechText = t('GAME_STARTED', playersText);
        const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
        sessionAttributes[PLAYER_COUNT_KEY] = numberOfPlayers;
        handlerInput.attributesManager.setSessionAttributes(sessionAttributes);

        return handlerInput.responseBuilder
            .speak(speechText)
            .withSimpleCard(t('SKILL_NAME'), speechText)
            .withShouldEndSession(false)
            .getResponse();
    }
};