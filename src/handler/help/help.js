// @ƒlow

import type {HandlerInput, ResponseBuilder} from '../../own-flow-types/ask-sdk/inputHandler';

export const HelpHandler = {
    canHandle(handlerInput: HandlerInput): boolean {
        const request = handlerInput.requestEnvelope.request;
        return request.type === 'IntentRequest'
            && request.intent.name === 'AMAZON.HelpIntent';
    },
    handle(handlerInput: HandlerInput): ResponseBuilder {
        return handlerInput.responseBuilder
            .speak('Hilfe kommt')
            .getResponse();
    },
};