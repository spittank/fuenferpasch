// @flow

export const deepGetOrDefault = (object: any, defaultReturnValue: any, ...args: any):any => {
    if (args.length === 1) {
        return object && typeof object[args[0]] !== 'undefined' ? object[args[0]] : defaultReturnValue;
    } else {
        const [_, ...rest] = args;
        return typeof object === 'undefined' ? defaultReturnValue : deepGetOrDefault(object[args[0]], defaultReturnValue, ...rest);
    }
};