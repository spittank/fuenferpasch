// @flow

export function ensure<T>(subject: ?T): T {
    if (typeof subject !== 'undefined' && subject !== null) {
        return subject;
    }
    throw new Error('Es wurde versucht auf einen Wert zuzugreifen, der null oder undefined ist.');
}