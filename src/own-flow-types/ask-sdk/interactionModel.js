// @flow


export type SlotModel = {
    name: string,
    type: string
};

export type IntentModel = {
    name: string,
    slots: SlotModel[],
    samples: string[]
};

export type SlotTypeName = {
    value: string
};

export type SlotTypeValue = {
    name: SlotTypeName
};

export type SlotType = {
    name: string,
    values: SlotTypeValue[]
};

export type LanguageModel = {
    invocationName: string,
    intents: IntentModel[],
    types: SlotType[]
};

export type PromptReference = {
    elicitation: string,
    confirmation: string
};

export type SlotDialogModel = {
    name: string,
    type: string,
    confirmationRequired: boolean,
    elicitationRequired: boolean,
    prompts: PromptReference
};

export type IntentDialogModel = {
    name: string,
    confirmationRequired: boolean,
    prompts: any,
    slots: SlotDialogModel[]
};

export type PromptVariant = {
    type: 'PlainText' | string,
    value: string
};

export type PromptModel = {
    id: string,
    variations: PromptVariant[]
};

export type DialogModel = {
    intents: IntentDialogModel[],
    prompts: PromptModel[]
};

export type InteractionModel = {
    languageModel: LanguageModel,
    dialog: DialogModel,
    prompts: any
};