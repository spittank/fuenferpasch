// @flow
import {AlexaListItem, AlexaListsMetadata, CreateListItemRequest} from 'ask-sdk-model';

type ConfirmationStatus = 'NONE' | 'CONFIRMED' | 'DENIED';

export type Attributes = {
    [string]: any
};

export type Slot = {
    name: string,
    value: string | boolean,
    confirmationStatus?: ConfirmationStatus,
    soure?: string
};

export type Slots = {
    [string]: Slot
};

export type Intent = {
    name: string,
    confirmationStatus?: ConfirmationStatus,
    slots: Slots
};

type RequestBase = {
    type: string
};

export type LaunchRequest = {
    type: 'LaunchRequest'
};

export type IntentRequest = {
    type: 'IntentRequest',
    intent: Intent
}

export type SessionEndRequest = {
    type: 'SessionEndRequest',
    reason: string
}

export type Request = LaunchRequest | IntentRequest | SessionEndRequest;

export type System = {
    apiAccessToken?: string
};

export type Context = {
    System: System
};

export type RequestEnvelope = {
    request: Request,
    context: Context
};

export type AttributesManager = {
    getSessionAttributes: () => Attributes,
    setSessionAttributes: Attributes => void,
    getPersistentAttributes: () => Promise<Attributes>,
    setPersistentAttributes: Attributes => void,
    savePersistentAttributes: () => Promise<void>
};

export type ApiConfiguration = {
    apiEndpoint: string
};

export type ListManagementServiceClient = {
    getListsMetadata(): Promise<AlexaListsMetadata>,
    createListItem(listId: string, createListItemRequest: CreateListItemRequest): Promise<AlexaListItem>;
};

export type ServiceClientFactory = {
    apiConfiguration: ApiConfiguration,
    getListManagementServiceClient: () => ListManagementServiceClient
};

export type HandlerInput = {
    requestEnvelope: RequestEnvelope,
    attributesManager: AttributesManager,
    serviceClientFactory?: ServiceClientFactory,
    responseBuilder: ResponseBuilder
};

export type ResponseBuilder = {
    speak: string => ResponseBuilder,
    reprompt: string => ResponseBuilder,
    withSimpleCard: (title: string, cardText: string) => ResponseBuilder,
    withShouldEndSession: boolean => ResponseBuilder,
    getResponse: () => ResponseBuilder,
    withAskForPermissionsConsentCard: (permissions: string[]) => ResponseBuilder,
    addDelegateDirective(updatedIntent: ?Intent): ResponseBuilder
};

export type Response = ResponseBuilder;

export interface RequestInterceptor {
    process(handlerInput: HandlerInput): Promise<void> | void;
}