// @flow
import {ExitHandler} from './handler/skillLifecycle/Exit';
import {HelpHandler} from './handler/help/help';
import {LaunchRequestHandler} from './handler/skillLifecycle/launchRequest';
import {SessionEndedRequestHandler} from './handler/skillLifecycle/sessionEndedRequest';
import {ErrorHandler} from './handler/skillLifecycle/Error';
import * as Alexa from 'ask-sdk';
import {StarteSpielIntentHandler} from './handler/game/starteSpiel';
import {WieVieleSpielerIntentHandler} from './handler/game/wieVieleSpieler';
import type {RequestInterceptor} from './own-flow-types/ask-sdk/inputHandler';
import {PersistenceAdapter} from 'ask-sdk-core';
import {SKILL_INTERNAL_NAME} from './consts';

export const createSkill = (PersistenceAdapterClass: Class<PersistenceAdapter>, requestInterceptors: RequestInterceptor[]) => {
    const skillBuilder = Alexa.SkillBuilders.custom();
    const persistenceAdapter = new PersistenceAdapterClass({
        tableName: `${SKILL_INTERNAL_NAME}_state`,
        createTable: true
    });
    const skill = skillBuilder
        .addRequestHandlers(
            ExitHandler,
            HelpHandler,
            LaunchRequestHandler,
            SessionEndedRequestHandler,
            StarteSpielIntentHandler,
            WieVieleSpielerIntentHandler
        )
        .withPersistenceAdapter(persistenceAdapter)
        .addErrorHandlers(ErrorHandler);
    if (requestInterceptors.length > 0) {
        skill.addRequestInterceptors(...requestInterceptors);
    }

    return skill.lambda();
};