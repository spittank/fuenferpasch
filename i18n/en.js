export const strings_en = {
    translation : {
        'SKILL_NAME': 'Five of a kind',
        'GAME_STARTED': 'Okay, I started a new game for %s players',
        'NO_OF_PLAYER': 'You are playing with %s players'
    }
};