export const strings_de = {
    translation : {
        'SKILL_NAME': 'Fünferpasch',
        'GAME_STARTED': 'Prima, ich habe ein neues Spiel für %s Mitspieler gestartet',
        'NO_OF_PLAYER': 'Du spielst mit %s Spielern'
    }
};