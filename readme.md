## Introduction

This is an example project to show how to do behavior driven development (BDD) and test driven development (TDD) of an Amazon Alexa Skill with [jest](https://jestjs.io/) and [cucumber.js](https://github.com/cucumber/cucumber-js).


![Testing an Alexa Skill with cucumber.js](https://blog.codecentric.de/files/2019/02/en-mock-voice-service-768x436.png "Testing an Alexa Skill with cucumber.js")


[English Documentation](readme.en.md)

[Deutsche Dokumentation](readme.de.md)