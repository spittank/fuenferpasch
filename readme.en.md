## Introduction

This is an example project to show behavior driven development (BDD) of an Amazon Alexa Skill with [cucumber.js](https://github.com/cucumber/cucumber-js).

The project is based on a series of blog entries. There is a tag in gitlab which reflects the state of development of the first blog entry.

## Blog-Einträge
|Blog Post | Gitlab Tag |
|----------|------------|
|Part 1: [BDD Alexa Skill with cucumber.js](https://blog.codecentric.de/en/2019/03/bdd-alexa-skill-cucumber-js-part-1/)|[Gitlab tag](https://gitlab.com/spittank/fuenferpasch/tags/BlogBDD)|
|Part 2: [Alexa Skill State Handling and cucumber.js](https://blog.codecentric.de/en/2019/03/behaviour-driven-development-bdd-alexa-skill-cucumber-js-part-2/)|[Gitlab tag](https://gitlab.com/spittank/fuenferpasch/tags/BlogBDD_EN_Part2)|

## Projektstruktur

* ``/src``: Root of all source foles
  * `/handler`: contains the handler functions of the various skill requests.
  * `/own`-flow-types: manually typed flow types for 3rd party components without flow support
  * `/test`: Test helper
  * Unit tests are spec files right next to the files being tested

* ``/build``: the bundled output file is 'build/main.js'
* ``/feature``: Cucumber Tests  
  * ``/step_definitions``: implementation of the different cucumber steps
* ``/flow-typed``: Flow-Types integrated via flow-typed
* ``/voiceUi``: The voice interaction models as defined in the Alexa Developer Console. Needs to be synced manually. 

## Linting
Execute eslint with:
``yarn lint``

## Test
Execute the jest-based unit tests with:
``yarn test:unit``

Execute the cucumber.js based acceptance tests with:
``yarn test:cucumber``

## Type Checking 
Execute the flow typechecker with: ``yarn flow``
