module.exports = {
    'rootDir': './',
    'verbose': true,
    'collectCoverage': true,
    'collectCoverageFrom': [
        'src/**.js',
        '!src/own-flow-types'
    ],
    'coveragePathIgnorePatterns': [
        'node_modules/',
        "src/own-flow-types/",
        "src/test"
    ],
    'coverageDirectory': '<rootDir>/coverage',
    'moduleFileExtensions': [
        'js'
    ],
    'transform': {
        '^.+\\.js$': 'babel-jest'
    }
};