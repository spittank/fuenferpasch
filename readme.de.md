## Einleitung

Dies ist ein Beispiel-Projekt für Behaviour Driven Development (BDD) eines Alexa Skills mit [cucumber.js](https://github.com/cucumber/cucumber-js).

Das Projekt basiert auf einer Reihe von Blog-Einträgen, zu denen teilweise eigene Tags in gitlab existieren, die den jeweiligen Entwicklungsstand des Blog-Eintrags reflektieren.

## Blog-Einträge

|Blog Eintrag | Gitlab Tag |
|--------------|------------|
|[Teil 1: Initiales Setup](https://blog.codecentric.de/2019/02/bdd-alexa-skills-teil1/)||
|[Teil 2: Lokale Lambda Entwicklung](https://blog.codecentric.de/2019/02/alexa-skills-bdd-cucumber-2/)||
|[Teil 3: Unit Testing mit Jest](https://blog.codecentric.de/2019/02/alexa-skills-bdd-cucumber-3/)|[Gitlab tag](https://gitlab.com/spittank/fuenferpasch/tags/BlogPart3_TDD)|
|[Teil 4: BDD mit Cucumber.js](https://blog.codecentric.de/2019/03/alexa-skills-bdd-cucumber-4/)|[Gitlab tag](https://gitlab.com/spittank/fuenferpasch/tags/BlogPart4_BDD)|
|[Teil 5: Zustandbehandlung von Alexa Skills in Tests mit cucumber.js](https://blog.codecentric.de/2019/03/alexa-skills-bdd-cucumber-5/)|[Gitlab tag](https://gitlab.com/spittank/fuenferpasch/tags/BlogPart5_AttributeHandling)|

## Projektstruktur

* ``/src``: Root für alle Quelldateien
  * `/handler`: behinhaltet die Handler Funktionen für die verschiedenen Requests
  * `/own`-flow-types: selbstgeschriebene Flow-Types für 3rd Party Packages ohne flow types
  * `/test`: Hilfsmethoden für unit-Tests
  * Unit Tests (jest) liegen als .spec Dateien neben den zu testenden Dateien

* ``/build``: der generierte Output wird unter 'build/main.js' abgelegt
* ``/feature``: Cucumber Tests  
  * ``/step_definitions``: Die Implementierung der einzelnen cucumber Schritte
* ``/flow-typed``: Flow-Typen die über flow-typed integriert wurden
* ``/voiceUi``: Das im online Editor von Amazon definierte interactionModel. Muss momentan noch manuell synchronisiert werden. 

## Linting
Eslint ist konfiguriert und kann mit 
``yarn lint``
ausgeführt werden.

## Test
Für Handler und domain existieren jest-Tests. Ausführbar mit:
``yarn test:unit``

Um das Zusammenspiel des definierten Voice-Interaction Model mit den Handlern und der Domain integriert zu testen existieren cucumber tests, ausführbar mit:
``yarn test:cucumber``

## Typechecker 
Flow ist als Typechecker (noch nicht ganz flächendeckend) im Einsatz. Einige externe Abhängigkeiten habe ich manuell typisiert.

Ausführbar mit ``yarn flow``
