# language: en
Feature: Start a new game
  Background:
    Given the user has opened the skill

  Scenario: A new game can be started, the number of players is stored
    When the user says: Start a new game with 4 players
    Then Alexa replies with: Okay, I started a new game for 4 players
    When the user says: How many players do I have
    Then Alexa replies with: You are playing with 4 players