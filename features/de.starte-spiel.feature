# language: de
Funktionalität: Ein Spiel starten
  Grundlage:
    Angenommen der Anwender hat den Skill geöffnet

  Szenario: Ein neues Spiel kann gestartet werden, die Anzahl an Mitspielern wird gespeichert
    Wenn der Anwender sagt: Starte ein Spiel mit 4 Spielern
    Dann antwortet Alexa mit: Prima, ich habe ein neues Spiel für 4 Mitspieler gestartet
    Wenn der Anwender sagt: Wieviele Spieler gibt es
    Dann antwortet Alexa mit: Du spielst mit 4 Spielern