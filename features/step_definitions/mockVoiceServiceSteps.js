// @flow
import expect from 'expect';
import {Given, Then, When} from 'cucumber';
import type {Attributes, HandlerInput, IntentRequest, Request, RequestInterceptor, Slot} from '../../src/own-flow-types/ask-sdk/inputHandler';
import fs from 'fs';
import {createSkill} from '../../src/createSkill';
import type {IntentDialogModel} from '../../src/own-flow-types/ask-sdk/interactionModel';
import type {IntentInvocation} from '../../src/test/mockVoiceService/mockVoiceService';
import {findMatchingIntent, getAllUtterances} from '../../src/test/mockVoiceService/mockVoiceService';
import {simulateDeserialization} from '../../src/test/mockHandlerInput';
import {localizationInterceptor} from '../../src/localizationInterceptor';
import {getVoiceUiModel} from '../../src/test/mockVoiceService/mockInteractionModel';

function createSkillForTest(world) {
    class MockPersistenceAdapterClass {
        getAttributes: () => Promise<any>;
        saveAttributes: () => Promise<void>;
        attributes: any;

        constructor() {
            this.getAttributes = () => Promise.resolve(world.persistentAttributes);
            this.saveAttributes = (_, attributes) => {
                world.persistentAttributes = attributes;
                return Promise.resolve();
            };
        }
    }
    const requestInterceptor: RequestInterceptor = {
        process(handlerInput: HandlerInput) {
            // Wrap setSessionAttributes, so that we can save these Attributes in the test
            const orginalSetSessionAttributes = handlerInput.attributesManager.setSessionAttributes;
            handlerInput.attributesManager.setSessionAttributes = (attributes: Attributes) => {
                world.sessionAttributes = attributes;
                orginalSetSessionAttributes.call(handlerInput.attributesManager, attributes);
            }
        }
    };
    return createSkill(MockPersistenceAdapterClass, [localizationInterceptor, requestInterceptor])
}

async function userHasOpenedSkill(locale: string) {
    const json = fs.readFileSync('src/test/mockVoiceService/requestJsonTemplates/launchRequest.json', 'utf-8');
    const launchRequest = JSON.parse(json);
    launchRequest.request.locale = locale;

    this.skill = createSkillForTest(this);
    const world = this;
    await executeRequest(world, this.skill, launchRequest);
}
Given(/^der Anwender hat den Skill geöffnet$/, async function() {
    await userHasOpenedSkill.call(this, 'de');
});

Given(/^the user has opened the skill$/,  async function() {
    await userHasOpenedSkill.call(this, 'en');
});

function alexaReplies(expectedResponse) {
    expect(this.lastResult.response.outputSpeech.ssml).toEqual(`<speak>${expectedResponse}</speak>`);
}
Then(/^antwortet Alexa mit[:]? (.*)$/, alexaReplies);
Then(/^Alexa replies with[:]? (.*)$/, alexaReplies);

function alexaRepliesWithSentencesAndPauses(dataTable, pause) {
    const allSentences = dataTable.hashes().map(row => row['Satz']).join(`<break time="${pause}s" />`);
    expect(this.lastResult.response.outputSpeech.ssml).toEqual(`<speak>${allSentences}</speak>`);
}
Then(/^beantwortet Alexa die Anfrage mit folgenden Sätzen wobei sie zwischen jedem Satz eine Pause von (\d*) Sekunde\(n\) lässt:$/,
    alexaRepliesWithSentencesAndPauses);
Then(/^bAlexa replies with the following sentences and a pause of (\d*) seconds between each sentence:$/,
    alexaRepliesWithSentencesAndPauses);

function alexasReplyStartsWith(expectedResponse) {
    expect(this.lastResult.response.outputSpeech.ssml).toStartWith(`<speak>${expectedResponse}`);
}
Then(/^beginnt die Antwort von Alexa mit[:]? (.*)$/, alexasReplyStartsWith);
Then(/^Alexas reply starts with[:]? (.*)$/, alexasReplyStartsWith);


function asksAlexa(dialogRequest) {
    expect(this.lastResult.response.directives[0].type).toEqual('Dialog.Delegate');

    // Den Slot ermitteln, der durch einen Dialog befüllt worden muss
    const lastIntentName = this.lastRequest.request.intent.name;
    const voiceUiModel = getVoiceUiModel(this.skillLocale);
    const intentDialogModel: IntentDialogModel = voiceUiModel.interactionModel.dialog.intents.find(i => i.name === lastIntentName);
    const slotsWithElicitation = intentDialogModel.slots.filter(s => s.elicitationRequired);
    const lastRequest: IntentRequest = (this.lastRequest.request: any);

    const slotsUnfilled = slotsWithElicitation.filter(s => !lastRequest.intent.slots[s.name]);
    expect(slotsUnfilled.length).toBeGreaterThan(0);

    this.lastSlotElicitation = slotsUnfilled[0].name;

    const prompt = voiceUiModel.interactionModel.prompts.find(p => p.id === slotsUnfilled[0].prompts.elicitation);
    const promptSpeak = prompt.variations[0].value;

    expect(promptSpeak).toEqual(dialogRequest.replace('?', ''));
}
Then(/^fragt Alexa[:]? (.*)[?]*$/, asksAlexa);
Then(/^asks Alexa[:]? (.*)[?]*$/, asksAlexa);


function alexaAsksForConfirmation(dialogRequest) {
    expect(this.lastResult.response).toHaveDirectiveType('Dialog.Delegate');

    // Den Slot ermitteln, der durch einen Dialog befüllt worden muss
    const lastIntentName = this.lastRequest.request.intent.name;
    const voiceUiModel = getVoiceUiModel(this.skillLocale);

    const intentDialogModel: IntentDialogModel = voiceUiModel.interactionModel.dialog.intents.find(i => i.name === lastIntentName);
    const slotsWithConfirmation = intentDialogModel.slots.filter(s => s.confirmationRequired);
    const lastRequest: IntentRequest = (this.lastRequest.request: any);

    const slotsUnconfirmed = slotsWithConfirmation.filter(s => lastRequest.intent.slots[s.name].confirmationStatus !== 'CONFIRMED');
    expect(slotsUnconfirmed.length).toBeGreaterThan(0);

    const prompt = voiceUiModel.interactionModel.prompts.find(p => p.id === slotsUnconfirmed[0].prompts.confirmation);
    const promptSpeak = prompt.variations[0].value;

    const slotName = slotsUnconfirmed[0].name;
    const slotValue = lastRequest.intent.slots[slotName].value;

    this.lastSlotConfirmation = slotName;
    const promtSpeakWithSlotValueFromUpdatedIntent = promptSpeak.replace(`{${slotName}}`, slotValue).replace('?', '').toLowerCase();
    expect(promtSpeakWithSlotValueFromUpdatedIntent).toEqual(dialogRequest.replace('?', '').toLowerCase());
}

Then(/^vergewissert sich Alexa[:]? (.*)[?]*$/, alexaAsksForConfirmation);
Then(/^Alexa asks for a confirmation[:]? (.*)[?]*$/, alexaAsksForConfirmation);

async function theUserConfirms() {
    // Dieser Schritt setzt voraus, das Alexa vorher mit einem Dialog Confirmation Request geantwortet hat
    const newRequest = JSON.parse(JSON.stringify(this.lastRequest));
    newRequest.request.intent.slots[this.lastSlotConfirmation] = {
        ...newRequest.request.intent.slots[this.lastSlotConfirmation],
        confirmationStatus: 'CONFIRMED',
        source: 'USER'
    };
    this.lastRequest = newRequest;
    await executeRequest(this, this.skill, newRequest);
}
When(/^der Anwender diese Nachfrage bestätigt$/, theUserConfirms);
When(/^the user confirms the requests$/, theUserConfirms);


async function theUserDeniesTheConfirmation() {
    // Dieser Schritt setzt voraus, das Alexa vorher mit einem Dialog Confirmation Request geantwortet hat
    const newRequest = JSON.parse(JSON.stringify(this.lastRequest));
    newRequest.request.intent.slots[this.lastSlotConfirmation] = {
        ...newRequest.request.intent.slots[this.lastSlotConfirmation],
        confirmationStatus: 'DENIED',
        source: 'USER'
    };
    this.lastRequest = newRequest;
    await executeRequest(this, this.skill, newRequest);
}

When(/^der Anwender diese Nachfrage verneint$/, theUserDeniesTheConfirmation);
When(/^dthe user denies the confirmation$/, theUserDeniesTheConfirmation);


async function theUserRepliesToTheQuestion(slotValue) {
    // Dieser Schritt setzt voraus, das Alexa vorher mit einem Dialog Request geantwortet hat
    const newRequest = JSON.parse(JSON.stringify(this.lastRequest));
    newRequest.request.intent.slots[this.lastSlotElicitation] = {
        name: this.lastSlotElicitation,
        value: slotValue,
        confirmationStatus: 'NONE',
        source: 'USER'
    };
    this.lastRequest = newRequest;
    await executeRequest(this, this.skill, newRequest);
}

When(/^der Anwender die Frage mit[:]? (.*) beantwortet$/, theUserRepliesToTheQuestion);
When(/^the user replies with[:]? (.*) beantwortet$/, theUserRepliesToTheQuestion);

async function executeRequest(world, skill, request) {
    return new Promise((resolve) => {
        // Handle session attributes
        request.session.attributes = simulateDeserialization(world.sessionAttributes);
        skill(request, {}, (error, result) => {
            world.lastError = error;
            world.lastResult = result;
            resolve();
        });
    });
}

async function theUserSays(utterance, locale: string) {
    const voiceUiModel = getVoiceUiModel(locale);

    const allUtterances = getAllUtterances(voiceUiModel.interactionModel);

    const matchingIntent: ?IntentInvocation = findMatchingIntent(allUtterances, utterance);

    expect(allUtterances).toHaveMatchingIntentFor(utterance);
    if (!matchingIntent) return;

    const slots = matchingIntent && matchingIntent.slots.reduce((acc, cur: Slot) => ({
        ...acc,
        [cur.name]: {
            name: [cur.name],
            value: cur.value,
            confirmationStatus: 'NONE',
            source: 'USER'
        }
    }), {});

    const json = fs.readFileSync('src/test/mockVoiceService/requestJsonTemplates/intentRequest.json', 'utf-8');
    const intentRequest = JSON.parse(json);

    intentRequest.request.intent.name = matchingIntent.intentName;
    intentRequest.request.intent.slots = slots;
    intentRequest.request.locale = locale;

    this.lastRequest = intentRequest;

    await executeRequest(this, this.skill, intentRequest);
}

When(/^der Anwender sagt[:]? (.*)$/, async function(utterance) {
    await theUserSays.call(this, utterance, 'de');
});

When(/^the user says[:]? (.*)$/, async function(utterance) {
    await theUserSays.call(this, utterance, 'en');
});

When(/^der Anwender nicht innerhalb von acht Sekunden etwas sagt$/, function() {
    //Hier muss nichts gemacht werden. Der Step ist wichtig für Reprompts, diese werden aber direkt in
    //der original Response mitgeschickt
});
When(/^the user takes no action within eight seconds$/, function() {
    //Hier muss nichts gemacht werden. Der Step ist wichtig für Reprompts, diese werden aber direkt in
    //der original Response mitgeschickt
});


function alexaReprompts(expectedResponse) {
    expect(this.lastResult.response.reprompt.outputSpeech.ssml).toEqual(`<speak>${expectedResponse}</speak>`);
}
Then(/^reagiert Alexa mit einer zweiten Antwort[:]? (.*)$/, alexaReprompts);
Then(/^gives a second reply[:]? (.*)$/, alexaReprompts);


function theScreenTitleIs(title){
    const renderTemplate = this.lastResult.response.directives.find(d => d.type === 'Display.RenderTemplate');
    expect(renderTemplate.template.title).toEqual(title);
}
Then(/^auf dem Bildschirm steht der Titel[:]? (.*)$/, theScreenTitleIs);
Then(/^the screen title is[:]? (.*)$/, theScreenTitleIs);

function theScreenDisplaysTheText(text) {
    const renderTemplate = this.lastResult.response.directives.find(d => d.type === 'Display.RenderTemplate');
    expect(renderTemplate.template.textContent.primaryText.text).toEqual(text);
}
Then(/^auf dem Bildschirm steht der Text[:]? (.*)$/, theScreenDisplaysTheText);
Then(/^the sreen displays the text[:]? (.*)$/, theScreenDisplaysTheText);

function theScreenDisplaysTheSentences(dataTable) {
    const allSentences = dataTable.hashes().map(row => row['Satz']).join(`<br />`);
    const renderTemplate = this.lastResult.response.directives.find(d => d.type === 'Display.RenderTemplate');
    expect(renderTemplate.template.textContent.primaryText.text).toEqual(allSentences);
}

Then(/^auf dem Bildschirm stehen die Sätze:$/, theScreenDisplaysTheSentences);
Then(/^the screen displays the sentences:$/, theScreenDisplaysTheSentences);