// @flow
import {setWorldConstructor} from 'cucumber';

export const resetWorld = function(world: any) {
    world.persistentAttributes = {};
    world.lastRequest = undefined;
    world.lastError = undefined;
    world.lastResult = undefined;
    world.lastResponse = undefined;
    world.sessionAttributes = {};
    world.skillLocale = 'de'; // The default language of the skill in tests
};

function skillWorld() {
    resetWorld(this);
}

setWorldConstructor(skillWorld);
