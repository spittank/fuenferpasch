// @flow
import expect from 'expect';
import {findMatchingIntent} from '../../src/test/mockVoiceService/mockVoiceService';
import {deepGetOrDefault} from '../../src/deepGetOrDefault';
import type {Utterance} from '../../src/test/mockVoiceService/mockVoiceService';

expect.extend({
    toHaveMatchingIntentFor(allUtterances: Utterance[], utterance: string) {
        const matchingIntent = findMatchingIntent(allUtterances, utterance);
        const pass = typeof matchingIntent !== 'undefined';
        return {
            message: () => !pass ? `No intent found handling utterance: "${utterance}". Remember to register new handlers in createSkill.js`
                : `Intent found handling utterance: "${utterance}" but none was expected`,
            pass
        };
    },
    toHaveDirectiveType(response, expectedType) {
        const directives = deepGetOrDefault(response, [], 'directives');
        const type = directives.length > 0 ? directives[0].type : '';
        const pass = type === expectedType;
        const speech = deepGetOrDefault(response, undefined, 'outputSpeech', 'ssml');
        const messageForSpeech = speech ? ` Reply was: ${speech}` : '';
        return {
            message: () => !pass ? `Expected response to have directive type ${expectedType}.${messageForSpeech}`
                : `Did not Expect response to have directive type ${expectedType}.${messageForSpeech}`,
            pass
        };
    },
    toStartWith(text: string, part: string) {
        const pass = text.startsWith(part);
        return {
            message: () => !pass ? `${text} doesn't start with ${part}`
                : `${text} starts with ${part} which was not expected`,
            pass
        };
    }
});