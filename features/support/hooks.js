import {Before} from 'cucumber';
import {resetWorld} from './world';

Before(function () {
    resetWorld(this);
});