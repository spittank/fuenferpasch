var path = require('path');
var webpack = require('webpack');
//var nodeExternals = require('webpack-node-externals');

const OUT_DIR = path.resolve(__dirname, 'build');
const SRC_DIR = path.resolve(__dirname, 'src');

module.exports = {
    target: 'node',
    entry: [ './src/index.js' ],
    output: {
        path: OUT_DIR,
        filename: 'main.js',
        library: 'main',
        libraryTarget: 'commonjs2',
    },
    externals: [ 'aws-sdk' ],
    plugins: [
        new webpack.IgnorePlugin(/^pg-native$/),
        new webpack.DefinePlugin({
            'process.env.BROWSER': false,
            __DEV__: process.env.NODE_ENV !== 'production',
        }),
    ],
    module: {
        rules: [
            {
                test: /\.(mjs|js|jsx)$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                options: {
                    configFile: './babel.config.js'
                }
            }
        ],
    },
    mode: 'production'//process.env.NODE_ENV === 'production' ? 'production' : 'development'
};
